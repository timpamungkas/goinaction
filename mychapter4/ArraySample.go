package mychapter4

import "fmt"

func ArrayValueSample() {
	fmt.Println("Array value start")
	array1 := [3]string{"Red", "Green", "Blue"}
	var array2 [3]string

	array2 = array1

	fmt.Println("Array value : original")
	fmt.Printf("array1 is %v, array2 is %v\n", array1, array2)

	fmt.Println("Array value : modifying array2")
	for i := 0; i < len(array2); i++ {
		array2[i] = "Modified-" + array2[i]
	}

	fmt.Println("Array value : modified array2")
	fmt.Printf("array1 is %v, array2 is %v\n", array1, array2)

	fmt.Println("Array value end")
}

func ArrayPointerSample() {
	fmt.Println("Array pointer start")
	arrayPtr1 := []*string{new(string), new(string), new(string)}
	var arrayPtr2 []*string

	*arrayPtr1[0] = "Brown"
	*arrayPtr1[1] = "Black"
	*arrayPtr1[2] = "Pink"

	arrayPtr2 = arrayPtr1

	fmt.Println("Array pointer : original")

	printer := func(array []*string) string {
		var output string
		for i := 0; i < len(array); i++ {
			output += *array[i] + " "
		}

		return output
	}

	fmt.Printf("array1 is %v, array2 is %v\n", printer(arrayPtr1), printer(arrayPtr2))

	fmt.Println("Array pointer : modifying")
	for i := 0; i < len(arrayPtr2); i++ {
		*arrayPtr2[i] = "Modified-" + *arrayPtr2[i]
	}

	fmt.Println("Array pointer : modified")
	fmt.Printf("array1 is %v, array2 is %v\n", printer(arrayPtr1), printer(arrayPtr2))

	fmt.Println("Array pointer end")
}

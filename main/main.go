package main

import (
	"fmt"
	"goinaction/dummy"
)

func init() {
	fmt.Println("Init function called")
}

func main() {
	f := dummy.Food{Flavor: "sweet", Name: "chocolate", Price: 10000}

	echo := func(s string) string {
		return fmt.Sprintf("This is an echo of %s", s)
	}

	fmt.Println(echo("A String"))
	fmt.Println(f)
}

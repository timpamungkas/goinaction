package main

import (
	"goinaction/mychapter4"
	"fmt"
)

func main() {
	mychapter4.ArrayValueSample()

	fmt.Println("------------------------------------------------------")

	mychapter4.ArrayPointerSample()
}

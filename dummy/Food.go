// Sample program for basic go introduction
package dummy

import "fmt"

// This is a documentation for Food struct
type Food struct {
	Name, Flavor string
	Price        float64
}

// This is a documentation for Food.String()
func (f Food) String() string {
	return fmt.Sprintf("Name: %s, Flavor: %s, Price: %.2f",
		f.Name, f.Flavor, f.Price)
}
